
# define WIN32_LEAN_AND_MEAN 0

# include "Strike7/Strike7dll.hpp"
# include "Strike7NightMode.hpp"

# include <windows.h>
# include <tchar.h>
# include <iostream>
# include <string>

int WINAPI _tWinMain( HINSTANCE	hInstance, HINSTANCE hPrevInstance, PTSTR lpCmdLine, int nCmdShow )
{
	UNREFERENCED_PARAMETER( hInstance );
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );
	UNREFERENCED_PARAMETER( nCmdShow );

	////////////////////////
	// Load applet icons //
	//////////////////////
	Strike7::Strike7ImagePtr pIcon1( new Strike7::Strike7Image( FIF_ICO, "Data\\30.ico" ) );

	///////////////////////
	// Load the API dll //
	/////////////////////
	Strike7::Strike7dll strike7dll( "Strike7API.dll" );

	///////////////////////
	// Load the applets //
	/////////////////////
	std::shared_ptr< Strike7NightMode > pApplet( new Strike7NightMode( L"Night Mode", pIcon1 ) );

	////////////////////////////////////////////
	// Register the applets with the api dll //
	//////////////////////////////////////////
	strike7dll.registerApplet( pApplet );

	pApplet->waitOnShutdown( );

	return 0;
}