#pragma once

/**
 * This is provided as is.
 *
 * This wraps the dll functions in a singleton class.
 * I know I'm not happy about the singleton either
 * but it will do for now.
 *
 * Author: Nicholas Welters
 * Data  : 01/06/2014
 */

# include "Strike7Applet.hpp"

# include <string>
# include <unordered_map>
# include <memory>

namespace Strike7
{
	class Strike7dll
	{
		private:
			std::string dllPath;
			static std::unordered_map< AppletID_t, std::shared_ptr< Strike7Applet > > appletMap;

			uint32_t supportedDevices[ 1 ];

			////////////////////
			// DLL interface //
			//////////////////
			HMODULE dll;

			OpenApplet  openApplet;

			RegisterAppletActiveEvent   registerAppletActiveEvent;
			RegisterAppletInactiveEvent registerAppletInactiveEvent;
			RegisterAppletTouchEvent    registerAppletTouchEvent;
			RegisterAppletResetEvent    registerAppletResetEvent;

			Strike7API apiFunctions;

		private:
			// API call back functions
			static void __stdcall appletActiveEvent  ( AppletID_t appletID, uint16_t endPointID, uint32_t endPointType );
			static void __stdcall appletInactiveEvent( AppletID_t appletID, uint16_t endPointID );
			static void __stdcall appletTouchEvent   ( AppletID_t appletID, uint16_t endPointID, uint32_t x, uint32_t y, bool pressed );
			static void __stdcall appletResetEvent   ( AppletID_t appletID, uint16_t endPointID );

			bool initalise( );

		public:
			Strike7dll( const std::string & dllPath );
			~Strike7dll( );

			bool registerApplet( std::shared_ptr< Strike7Applet > pApplet );
	};
}
