# include "Strike7dll.hpp"

namespace Strike7
{
	std::unordered_map< AppletID_t, std::shared_ptr< Strike7Applet > > Strike7dll::appletMap;

	Strike7dll::Strike7dll( const std::string & dllPath )
		: dllPath( dllPath )
		, dll( nullptr )
		, apiFunctions( )
	{
		supportedDevices[ 0 ] = 0;
	}


	Strike7dll::~Strike7dll( )
	{
		if( dll )
		{
			FreeLibrary( dll );
		}
	}

	/**
	 * Loads the strike 7 api dll and obtains pointers to the functions held with in the dll.
	 * 
	 * @return The success of loading the strike 7 api dll.
	 */
	bool Strike7dll::initalise( )
	{
		static bool init = false;

		if( !init )
		{
			dll = LoadLibraryA( dllPath.c_str( ) );

			if( dll == nullptr )
			{
				printf( "Failed to load library %s\n", dllPath.c_str( ) );
				return false;
			}

			registerAppletActiveEvent = ( RegisterAppletActiveEvent ) GetProcAddress( dll, "RegisterAppletActiveEvent" );
			registerAppletInactiveEvent = ( RegisterAppletInactiveEvent ) GetProcAddress( dll, "RegisterAppletInactiveEvent" );
			registerAppletTouchEvent = ( RegisterAppletTouchEvent ) GetProcAddress( dll, "RegisterAppletTouchEvent" );
			registerAppletResetEvent = ( RegisterAppletResetEvent ) GetProcAddress( dll, "RegisterAppletResetEvent" );

			openApplet = ( OpenApplet ) GetProcAddress( dll, "OpenApplet" );
			apiFunctions.closeApplet = ( CloseApplet ) GetProcAddress( dll, "CloseApplet" );

			apiFunctions.appletUpdateScreen = ( AppletUpdateScreen ) GetProcAddress( dll, "AppletUpdateScreen" );
			apiFunctions.appletScroll = ( AppletScroll ) GetProcAddress( dll, "AppletScroll" );
			apiFunctions.appletDraw = ( AppletDraw ) GetProcAddress( dll, "AppletDraw" );
			apiFunctions.appletFill = ( AppletFill ) GetProcAddress( dll, "AppletFill" );
			apiFunctions.appletDrawText = ( AppletDrawText ) GetProcAddress( dll, "AppletDrawText" );
		}

		return dll != nullptr;
	}

	/**
	 * This will initialize the dll and obtain pointers to the interface if this has not been done.
	 * This will the register the applet to the api so that applet classes can interact with the api.
	 *
	 * @param pApplet The applet to register to the strike 7 api so that the applet can interact with the system.
	 * 
	 * @return The success of registering the applet with the api dll.
	 */
	bool Strike7dll::registerApplet( std::shared_ptr< Strike7Applet > pApplet )
	{
		if( initalise( ) )
		{
			AppletID_t appletID;
			appletID = openApplet( pApplet->getName( ).c_str( ), pApplet->getIcon( )->getWidth( ), pApplet->getIcon( )->getHeight( ), pApplet->getIcon( )->getBits( ), supportedDevices );
			
			registerAppletActiveEvent( &Strike7dll::appletActiveEvent, appletID );
			registerAppletInactiveEvent( &Strike7dll::appletInactiveEvent, appletID );
			registerAppletTouchEvent( &Strike7dll::appletTouchEvent, appletID );
			registerAppletResetEvent( &Strike7dll::appletResetEvent, appletID );
			
			pApplet->initialise( appletID, apiFunctions );

			appletMap.emplace( appletID, pApplet );

			wprintf( L"Registered %s with ID %d\n", pApplet->getName( ).c_str( ), appletID );

			return true;
		}

		wprintf( L"Failed to register applet %s \n", pApplet->getName( ).c_str( ) );

		return false;
	}

	//////////////////////////////
	// API call back functions //
	////////////////////////////
	void Strike7dll::appletActiveEvent( AppletID_t appletID, uint16_t endPointID, uint32_t endPointType )
	{
		appletMap[ appletID ]->appletActiveEvent( endPointID, endPointType );
	}

	void Strike7dll::appletInactiveEvent( AppletID_t appletID, uint16_t endPointID )
	{
		appletMap[ appletID ]->appletInactiveEvent( endPointID );
	}

	void Strike7dll::appletTouchEvent( AppletID_t appletID, uint16_t endPointID, uint32_t x, uint32_t y, bool pressed )
	{
		appletMap[ appletID ]->appletTouchEvent( endPointID, x, y, pressed );
	}

	void Strike7dll::appletResetEvent( AppletID_t appletID, uint16_t endPointID )
	{
		appletMap[ appletID ]->appletResetEvent( endPointID );
	}
}
