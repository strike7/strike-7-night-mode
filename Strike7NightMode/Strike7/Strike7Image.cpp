#include "Strike7Image.hpp"


namespace Strike7
{
	Strike7Image::Strike7Image( FREE_IMAGE_FORMAT FIF, std::string fileLocation, int flags )
		: pImage( nullptr )
		, width( 0 )
		, height( 0 )
	{
		pImage = FreeImage_Load( FIF, fileLocation.c_str( ), flags );
		
		if( pImage )
		{
			FreeImage_FlipVertical( pImage );

			width = FreeImage_GetWidth( pImage );
			height = FreeImage_GetHeight( pImage );
		}
		else
		{
			printf( "ERROR: Image not found - %s\n", fileLocation.c_str( ) );
		}
	}


	Strike7Image::~Strike7Image( )
	{
		if( pImage )
		{
			FreeImage_Unload( pImage );
		}
	}


	uint32_t Strike7Image::getWidth( )
	{
		return width;
	}

	uint32_t Strike7Image::getHeight( )
	{
		return height;
	}

	BYTE * Strike7Image::getBits( )
	{
		return FreeImage_GetBits( pImage );
	}
}