# pragma once

/**
* This is provided as is.
*
* This provides an iterface for interacting with the strike 7 api.
* This should make it easier to make applets in c++ as we were lucky
* enough not to get a c++ example.
*
* Author: Nicholas Welters
* Data  : 01/06/2014
*/

# include "Strike7API.hpp"
# include "Strike7Image.hpp"

# include <string>

# include <iomanip>
# include <sstream>

namespace Strike7
{
	class Strike7Applet
	{
		private:
			///////////////////////
			// Applet Functions //
			/////////////////////
			Strike7API apiFunctions;

			//////////////////
			// Applet Data //
			////////////////
			std::wstring name;
			Strike7ImagePtr icon;

			AppletID_t appletID;

		public:
			Strike7Applet( const std::wstring & name, const Strike7ImagePtr & icon );
			virtual ~Strike7Applet( );

			std::wstring & getName( );
			Strike7ImagePtr & getIcon( );
			AppletID_t getID( );

			void initialise( const AppletID_t & appletID, const Strike7API & functions );

			////////////////
			// CallBacks //
			//////////////
			virtual void appletActiveEvent  ( uint16_t endPointID, uint16_t endPointType ) = 0;
			virtual void appletInactiveEvent( uint16_t endPointID ) = 0;
			virtual void appletTouchEvent   ( uint16_t endPointID, uint32_t x, uint32_t y, bool pressed ) = 0;
			virtual void appletResetEvent   ( uint16_t endPointID ) = 0;

			//////////////
			// CallsTo //
			////////////
			void AppletUpdateScreen( uint16_t endPointID );
			void AppletScroll  ( uint16_t endPointID, uint32_t x, uint32_t y, uint32_t w, uint32_t h, int32_t dx, int32_t dy );
			void AppletDraw    ( uint16_t endPointID,  int32_t x,  int32_t y, uint32_t w, uint32_t h, ImageBGRA * data );
			void AppletFill    ( uint16_t endPointID,  int32_t x,  int32_t y, uint32_t w, uint32_t h, uint32_t colour );
			void AppletDrawText( uint16_t endPointID,  int32_t x,  int32_t y, uint32_t size, uint32_t colour, const TCHAR* text );
	};
}
