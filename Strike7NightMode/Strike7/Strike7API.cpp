# include "Strike7API.hpp"

namespace Strike7
{
	Strike7API::Strike7API( )
		: closeApplet( nullptr )
		, appletUpdateScreen( nullptr )
		, appletScroll( nullptr )
		, appletDraw( nullptr )
		, appletFill( nullptr )
		, appletDrawText( nullptr )
	{ }


	Strike7API::~Strike7API( )
	{ }
}
