#pragma once

# include "Strike7\Strike7Applet.hpp"
# include <condition_variable>

class Strike7NightMode : public Strike7::Strike7Applet
{
	private:
		std::condition_variable shutdown;

	public:
		Strike7NightMode( const std::wstring & name, const Strike7::Strike7ImagePtr & icon );
		virtual ~Strike7NightMode( );

		void waitOnShutdown( );

		////////////////
		// CallBacks //
		//////////////
		virtual void appletActiveEvent( uint16_t endPointID, uint16_t endPointType );
		virtual void appletInactiveEvent( uint16_t endPointID );
		virtual void appletTouchEvent( uint16_t endPointID, uint32_t x, uint32_t y, bool pressed );
		virtual void appletResetEvent( uint16_t endPointID );
};

