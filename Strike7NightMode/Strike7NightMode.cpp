#include "Strike7NightMode.hpp"


Strike7NightMode::Strike7NightMode( const std::wstring & name, const Strike7::Strike7ImagePtr & icon )
	: Strike7Applet( name, icon )
	, shutdown( )
{ }


Strike7NightMode::~Strike7NightMode( )
{ }

void Strike7NightMode::waitOnShutdown( )
{
	std::mutex _;
	std::unique_lock< std::mutex > lock( _ );
	shutdown.wait( lock );
}

////////////////
// Callbacks //
//////////////
void Strike7NightMode::appletActiveEvent( uint16_t endPointID, uint16_t endPointType )
{
	AppletDrawText( endPointID, 380, -30, 150, 0x0f0000, L"X" );
	AppletUpdateScreen( endPointID );
}

void Strike7NightMode::appletInactiveEvent( uint16_t endPointID )
{

}

void Strike7NightMode::appletTouchEvent( uint16_t endPointID, uint32_t x, uint32_t y, bool pressed )
{
	if( x > 380 && y < 100 )
	{
		shutdown.notify_all( );
	}
	else
	{
		AppletDrawText( endPointID, 380, -30, 150, 0x0f0000, L"X" );
		AppletUpdateScreen( endPointID );
	}
}

void Strike7NightMode::appletResetEvent( uint16_t endPointID )
{

}